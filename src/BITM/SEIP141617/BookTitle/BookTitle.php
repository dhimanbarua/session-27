<?php
namespace App\BookTitle;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;
class BookTitle extends DB
{
    public $id="";

    public $book_title="users";

    public $author_name="";

    public function __construct()
    {
        parent::__construct();
    }
    public function setData($data=NULL)
    {
        if(array_key_exists('id',$data))
        {
            $this->id = $data['id'];
        }
        if(array_key_exists('book_title',$data))
        {
            $this->book_title = $data['book_title'];
        }
        if(array_key_exists('author_name',$data))
        {
            $this->author_name = $data['author_name'];
        }
    }
     public function store()
    {
        $arrData = array($this->book_title,$this->author_name);
        $conn=$this->DBH;
        $STH=$conn->prepare("INSERT INTO book_title(book_title,author_name) VALUES(?,?) ");
        $STH->execute($arrData);

        if($STH)
            {
                Message::message("<div id='msg'><h3 align='center'>[BookTitle: $this->book_title],[AuthorName:$this->author_name]
                    <br>Date Has been Inserted Successfully!!!!!!</h3></div> ");
            }
        else
            {
                Message::message("<div id='msg'><h3 align='center'>[BookTitle: $this->book_title],[AuthorName:$this->author_name]
                    <br>Date Has Not been Inserted Successfully!!!!!!</h3></div> ");
            }
        Utility::redirect('create.php');

    }// end of Store();

    public function index($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from book_title ORDER BY book_title DESC');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();

    public function view($fetchMode='ASSOC'){

        $sql = 'SELECT * from book_title where id='.$this->id;

        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();

    public function update(){
        $arrData = array($this->book_title, $this->author_name);

        $sql = "UPDATE book_title SET book_title = ?, author_name = ? WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);

        Utility::redirect("index.php");
    }// end of update

    public function delete(){
        $sql = "DELETE FROM book_title WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();
        Utility::redirect('index.php');
    }// end of delete

    public function trash(){
        $sql = "UPDATE book_title SET is_deleted=NOW() WHERE id=".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute();

        Utility::redirect('index.php');
    }// end of trash



}