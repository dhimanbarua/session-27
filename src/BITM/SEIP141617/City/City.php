<?php
namespace App\City;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;
class City extends DB
{
    public $id="";

    public $city="";

    public $name="";

    public function __construct()
    {
        parent::__construct();
    }
    public function setData($data=NULL)
    {
        if(array_key_exists('id',$data))
        {
            $this->id = $data['id'];
        }
        if(array_key_exists('name',$data))
        {
            $this->name= $data['name'];
        }
        if(array_key_exists('city',$data))
        {
            $this->city = $data['city'];
        }
    }
    public function store()
    {
        $arrData = array($this->name,$this->city);
        $conn=$this->DBH;
        $STH=$conn->prepare("INSERT INTO city(name,city) VALUES(?,?) ");
        $STH->execute($arrData);

        if($STH)
        {
            Message::message("<div id='msg'><h3 align='center'>[User Name: $this->name],[City:$this->city]
                    <br>Data Has been Inserted Successfully!!!!!!</h3> </div> ");
        }
        else
        {
            Message::message("<div id='msg'><h3 align='center'>[User Name: $this->name],[City:$this->city]
                    <br>Data Has Not been Inserted Successfully!!!!!!</h3></div> ");
        }
        Utility::redirect('create.php');

    }

    public function index(){
        $STH = $this->DBH->query("SELECT * FROM city ORDER BY city DESC");

        $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData = $STH->fetchAll();

        return $arrAllData;
    }


}