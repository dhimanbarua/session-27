<?php
namespace App\Gender;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;
class Gender extends DB
{
    public $id="";

    public $gender="";

    public $name="";

    public function __construct()
    {
        parent::__construct();
    }

    public function setData($data=NULL)
    {
        if(array_key_exists('id',$data))
        {
            $this->id = $data['id'];
        }
        if(array_key_exists('name',$data))
        {
            $this->name= $data['name'];
        }
        if(array_key_exists('gender',$data))
        {
            $this->gender = $data['gender'];
        }
    }
    public function store()
    {
        $arrData = array($this->name,$this->gender);
        $conn=$this->DBH;
        $STH=$conn->prepare("INSERT INTO gender(name,gender) VALUES(?,?) ");
        $STH->execute($arrData);

        if($STH)
        {
            Message::message("<div id='msg'><h3 align='center'>[User Name: $this->name],[Gender:$this->gender]
                    <br>Data Has been Inserted Successfully!!!!!!</h3></div> ");
        }
        else
        {
            Message::message("<div id='msg'><h3 align='center'>[User Name: $this->name],[Gender:$this->gender]
                    <br>Data Has Not been Inserted Successfully!!!!!!</h3> </div>");
        }
        Utility::redirect('create.php');

    }


}