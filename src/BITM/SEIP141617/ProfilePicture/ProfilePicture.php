<?php
namespace App\ProfilePicture;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;
class ProfilePicture extends DB
{
    public $id="";

    public $name="";

    public $profilePicture="";

    public function __construct()
    {
        parent::__construct();
        //echo "Hello";
    }
    public function setData($data=NULL)
    {
        if(array_key_exists('id',$data))
        {
            $this->id = $data['id'];
        }
        if(array_key_exists('name',$data))
        {
            $this->name= $data['name'];
        }
        if(array_key_exists('profilePicture',$data))
        {
            $this->profilePicture = $data['profilePicture'];
        }
    }
    public function store()
    {
        $arrData = array($this->name,$this->profilePicture);
        $conn=$this->DBH;
        $STH=$conn->prepare("INSERT INTO profilepic(name,profilePicture) VALUES(?,?) ");
        $STH->execute($arrData);

        if($STH)
        {
            Message::message("<div id='msg'><h3 align='center'>[User Name: $this->name],[Profile Picture:$this->profilePicture]
                    <br>Data Has been Inserted Successfully!!!!!!</h3></div> ");
        }
        else
        {
            Message::message("<div id='msg'><h3 align='center'>>[User Name: $this->name],[Profile Picture:$this->profilePicture]
                    <br>Data Has Not been Inserted Successfully!!!!!!</h3></div> ");
        }
        Utility::redirect('create.php');
    }

    public function index(){
        $STH = $this->DBH->query('SELECT * FROM profilepic');
        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrAllData = $STH->fetchAll();

        return $arrAllData;
    }


}