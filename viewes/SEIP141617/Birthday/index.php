<head>
    <title>Birthday</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>


<?php
require_once('../../../vendor/autoload.php');

use App\Birthday\Birthday;
use App\Message\Message;

$objBirthday = new Birthday();
$allData = $objBirthday->index();

?>
<table border="5">
    <tr>
        <th>Sl</th>
        <th>ID</th>
        <th>Name</th>
        <th>Birthday</th>
        <th>Action</th>
    </tr>
    <?php
    $sl = 1;
    foreach($allData as $oneData){ ?>

        <tr>
            <td><?php echo $sl++; ?></td>
            <td><?php echo $oneData['id'] ?></td>
            <td><?php echo $oneData['name'] ?></td>
            <td><?php echo $oneData['birthday'] ?></td>
            <td><?php
                echo "<a href='view.php?id=".$oneData['id']."'><button class='btn btn-info'>View</button></a> ";
                echo "<a href='view.php?id=".$oneData['id']."'><button class='btn btn-primary'>Edit</button></a> ";
                echo "<a href='view.php?id=".$oneData['id']."'><button class='btn btn-success'>Trash</button></a> ";
                echo "<a href='view.php?id=".$oneData['id']."'><button class='btn btn-danger'>Delete</button></a> ";
            ?></td>
        </tr>
   <?php } ?>
</table>

