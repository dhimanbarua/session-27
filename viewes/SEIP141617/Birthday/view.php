<?php
require_once('../../../vendor/autoload.php');
use App\Birthday\Birthday;

$objBirthday = new Birthday();
$objBirthday->setData($_GET);
$oneData = $objBirthday->view();
?>
<table border="3">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Birthday</th>
    </tr>
    <tr>
        <td><?php echo $oneData['id'] ?></td>
        <td><?php echo $oneData['name'] ?></td>
        <td><?php echo $oneData['birthday'] ?></td>
    </tr>
</table>