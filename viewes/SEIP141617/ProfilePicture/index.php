<head>
    <title>Profile Picture</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<?php
require_once('../../../vendor/autoload.php');

use App\ProfilePicture\ProfilePicture;
use App\Message\Message;

$objprofilePicture = new ProfilePicture();
$allData = $objprofilePicture->index();

$serial = 1;
echo "<table class='table-class' border='5px'>";

echo "<th> Serial </th>";
echo "<th> ID </th>";
echo "<th> Name </th>";
echo "<th> Profile Picture </th>";


foreach($allData as $oneData){
    echo "<tr style='height: 40px'>";
    echo "<td>".$serial."</td>";

    echo "<td>".$oneData->id."</td>";
    echo "<td>".$oneData->name."</td>";
    echo "<td>".$oneData->profilePicture."</td>";


    echo "<td>";

    echo "<a href='view.php?id=$oneData->id'><button class='btn btn-info'>View</button></a> ";
    echo "<a href='edit.php?id=$oneData->id'><button class='btn btn-primary'>Edit</button></a> ";
    echo "<a href='delete.php?id=$oneData->id'><button class='btn btn-danger'>Delete</button></a> ";


    echo "</td>";

    echo "</tr>";

    $serial++;
}

echo "</table>";